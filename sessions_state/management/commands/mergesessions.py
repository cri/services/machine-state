from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from psycopg2.extras import DateTimeTZRange

from datetime import timedelta

from ... import models


class Command(BaseCommand):
    help = "Merge session entries"

    def handle(self, *args, **options):
        timeout = timedelta(seconds=settings.SESSIONS_MERGE_TIMEOUT_SECONDS)
        qs = models.SessionState.objects.filter(
            timestamp__lt=timezone.now() - timeout
        ).order_by("ip", "login", "timestamp")

        last_sessions = []

        def new_session(last_states):
            first_state = last_states[0]
            last_state = last_states[-1]
            return models.Session(
                hostname=first_state.hostname,
                ip=first_state.ip,
                login=first_state.login,
                interval=DateTimeTZRange(
                    lower=first_state.timestamp,
                    upper=last_state.timestamp + timedelta(seconds=1),
                ),
            )

        last_states = []
        for entry in qs:
            if last_states and not last_states[-1].is_same_session(entry):
                first_state = last_states[0]
                last_sessions.append(new_session(last_states))
                last_states = []
            last_states.append(entry)
        last_sessions.append(new_session(last_states))

        sessions_to_save = models.Session.prepare_merge_sessions(last_sessions)

        with transaction.atomic():
            for session in sessions_to_save:
                session.save()
            qs.delete()
