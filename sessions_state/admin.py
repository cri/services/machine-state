from django.contrib import admin

from . import models


@admin.register(models.SessionState)
class SessionStateAdmin(admin.ModelAdmin):
    list_display = ("hostname", "login", "ip", "timestamp")
    list_display_links = list_display
    list_filter = ("hostname", "login", "ip")
    search_fields = ("hostname", "login", "ip")


@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ("hostname", "login", "ip", "interval")
    list_display_links = list_display
    list_filter = ("hostname", "login", "ip")
    search_fields = ("hostname", "login", "ip")
