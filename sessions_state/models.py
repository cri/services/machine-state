from django.contrib.postgres.fields import DateTimeRangeField
from django.db import models
from django.db.models import Q

from psycopg2.extras import DateTimeTZRange

from datetime import timedelta
import uuid


class SessionState(models.Model):
    SESSION_TIMEOUT_SECONDS = 3 * 60

    id = models.BigAutoField(primary_key=True, unique=True)

    timestamp = models.DateTimeField()

    hostname = models.CharField(max_length=255)
    ip = models.CharField(max_length=255)

    login = models.CharField(max_length=255)

    class Meta:
        db_table = "session_states"

    def __str__(self):
        return f"{self.timestamp} {self.hostname} {self.login}"

    def is_same_session(self, other):
        timeout = timedelta(seconds=self.SESSION_TIMEOUT_SECONDS)
        return all((self.login == other.login, self.ip == other.ip,)) and any(
            (
                self.timestamp - timeout <= other.timestamp <= self.timestamp + timeout,
                other.timestamp - timeout
                <= self.timestamp
                <= other.timestamp + timeout,
            )
        )


class Session(models.Model):
    SESSION_TIMEOUT_SECONDS = SessionState.SESSION_TIMEOUT_SECONDS

    hostname = models.CharField(max_length=255)
    ip = models.CharField(max_length=255)

    login = models.CharField(max_length=255)

    interval = DateTimeRangeField()

    class Meta:
        ordering = ("-interval", "login")

    def __str__(self):
        return f"{self.interval} {self.hostname} {self.login}"

    def merge_session(self, other):
        self.interval = DateTimeTZRange(
            lower=min(self.interval.lower, other.interval.lower),
            upper=max(self.interval.upper, other.interval.upper),
        )

    @classmethod
    def prepare_merge_sessions(cls, sessions):
        timeout = timedelta(seconds=cls.SESSION_TIMEOUT_SECONDS)
        sessions_to_save = []

        for session in sessions:
            existing_sessions = cls.objects.filter(
                login=session.login,
                ip=session.ip,
            ).filter(
                Q(interval__contains=session.interval.lower - timeout)
                | Q(interval__contains=session.interval.upper + timeout),
            )

            if existing_sessions:
                existing_session = existing_sessions[0]
                existing_session.merge_session(session)
                session = existing_session

            sessions_to_save.append(session)

        return sessions_to_save
