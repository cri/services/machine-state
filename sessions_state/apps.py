from django.apps import AppConfig


class SessionsStateConfig(AppConfig):
    name = "sessions_state"
