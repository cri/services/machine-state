#!/bin/sh

set -e

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ ! -z "$FILE_PATH" ]; then
    cat "$FILE_PATH"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

export DB_USER="$(getsecret "DB_USER")"
export DB_PASSWORD="$(getsecret "DB_PASSWORD")"

exec /app/machine-state
