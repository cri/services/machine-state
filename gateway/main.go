package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// model
type SessionState struct {
	ID uint `gorm:"primaryKey"`

	Timestamp time.Time `gorm:"autoCreateTime"`

	Hostname string
	Ip       string

	Login string
}

type SessionStateInput struct {
	Hostname string `json:"hostname"`
	Ip       string `json:"ip"`
	Login    string `json:"login"`
}

var DB *gorm.DB

func Getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func GetDSN() string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s %s",
		Getenv("DB_HOST", ""),
		Getenv("DB_PORT", "5432"),
		Getenv("DB_USER", ""),
		Getenv("DB_PASSWORD", ""),
		Getenv("DB_NAME", "machine_state"),
		Getenv("DB_EXTRA_CONFIG", ""),
	)
}

func ConnectDatabase() {
	dsn := GetDSN()

	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to the database")
	}

	DB = database
}

func SessionPing(c *gin.Context) {
	var input SessionStateInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if input.Hostname == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "hostname was empty"})
		return
	}
	if input.Ip == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ip was empty"})
		return
	}
	if input.Login == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "login was empty"})
		return
	}

	if input.Ip != c.ClientIP() {
		c.JSON(http.StatusBadRequest, gin.H{"error": "you are no who you say you are"})
		return
	}

	sessionState := SessionState{
		Hostname: input.Hostname,
		Ip:       input.Ip,
		Login:    input.Login,
	}

	result := DB.Create(&sessionState)

	if result.Error != nil {
		os.Exit(1)
	}

	c.String(http.StatusOK, "ok")
}

func Probe(c *gin.Context) {
	// TODO: support PROBES_IP
	err := DB.Exec("SELECT 1").Error
	if err != nil {
		c.String(http.StatusServiceUnavailable, "error: unavailable to query database")
		return
	}
	c.String(http.StatusOK, "ok")
}

func main() {
	router := gin.Default()

	ConnectDatabase()

	router.GET("/healthz", Probe)
	router.GET("/readiness", Probe)

	router.POST("/session/ping", SessionPing)

	router.Run()
}
